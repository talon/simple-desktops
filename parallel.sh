#!/bin/bash
# Download a file.txt of urls into a folder ($2 or derived from filename):
#   ./parallel.sh urls.txt && ls urls/
folder=${2:-${1%.*}}
mkdir -p "$folder" 
echo "downloading urls to $folder..."
for i in $(cat $1); do
   # run the curl job in the background so we can start another job
   # and disable the progress bar (-s)
   (cd "./$folder" && curl $i -o $(basename "$i") -s &)
done
wait # wait for all background jobs to terminate
echo "done!"
