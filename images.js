#!/usr/bin/env node
const {JSDOM} = require('jsdom')
// read HTML from stdin
const html = require('fs').readFileSync(process.stdin.fd, 'utf-8')
// current query selector for <img> tags on simpledesktops.com/browse/
const imgs = ".desktops img"
// parse image.src to stdout
for (let img of new JSDOM(html).window.document.querySelectorAll(imgs).values())
  console.log(img.src.replace(/\.[^\.]+\.png$/, ""))
