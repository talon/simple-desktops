# [simpledesktops.com](http://simpledesktops.com/)

## Usage

install JSDOM with `npm install`

Then download all the /browse/ pages HTML:
```sh
make pages # update Makefile to adjust number (currently 52)
```
And now you can download all the parsed image URLs to a simple-desktops folder:
```sh
make simple-desktops
```
Optionally add .zip for... that.

_(There's a race in breaking the ability to simply run `make simple-desktops`. I might get around to fixing it but probably not.)_

## How

The [/browse/{page}](http://simpledesktops.com/browse/1/) route lists ~28 images per page whose sources can be extracted from the DOM using:

```jsx
for (let img of document.querySelectorAll(".desktops img").values()) {
  console.log(img.src)
}
```

These URLs will have two extensions **{filename}.png***.295x184_q100.png.* The second part points to the thumbnail and can be stripped to get the URL for the full size image:

```jsx
img.src.replace(/\.[^\.]+\.png$/, "")
```

When a page number outside the range provided by the browser is hit the `querySelectorAll` will return an empty `NodeList` the ".more" button will also not be present. Iterating through pages until this happens will collect all the available image URLs.

jsdom enables piping the HTML into a JS file where the above snippets can be used to print the image URLs to stdout. The HTML and the images will be downloaded with `curl(1)`.

[https://github.com/jsdom/jsdom](https://github.com/jsdom/jsdom)
