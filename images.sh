#!/bin/bash
# create images.txt by reading every HTML file in ./pages and using
# images.js to extract the URLs from them.
touch images.txt
for page in ./pages/*; do
  cat "$page" | ./images.js | tee -a images.txt
done
