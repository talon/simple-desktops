pages.txt: 
	./pages.sh 52
pages: pages.txt
	./parallel.sh pages.txt
images.txt: pages
	./images.sh
simple-desktops: images.txt
	./parallel.sh images.txt simple-desktops
simple-desktops.zip: simple-desktops 
	sudo zip -r simple-desktops.zip simple-desktops
deploy: simple-desktops.zip 
	sudo cp ./simple-desktops.zip /etc/nginx/html/files/

.PHONY: clean
clean:
	rm -rf pages.txt pages images.txt images simple-desktops*
