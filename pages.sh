#!/bin/bash
# Create a text file of a certain number of browse urls:
#   ./pages.sh 52
# This is useful for preparing to download the pages with ./parallel.sh
SERVER="http://simpledesktops.com"
for i in $(seq $1); do
  echo "$SERVER/browse/${i}/" >> pages.txt
done
